﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeworkWinForm
{
    public class DataService
    {
        public SqlConnectionStringBuilder ConnectionStringBuilder(string login = "", string password = "")
        {
            SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
            sqlConnectionStringBuilder.DataSource = @"(LocalDB)\MSSQLLocalDB"; // строка подключения
            sqlConnectionStringBuilder.InitialCatalog = "WinFormDB"; // имя БД
            sqlConnectionStringBuilder.UserID = login;
            sqlConnectionStringBuilder.Password = password;
            return sqlConnectionStringBuilder;
        }

        public void DbConnect (SqlConnectionStringBuilder sqlConnectionStringBuilder) // передача готового экземпляра без создания нового
        {
            using (SqlConnection sqlConnection = new SqlConnection())
            {
                sqlConnection.ConnectionString = sqlConnectionStringBuilder.ConnectionString; // создаем строку подключения и 
                try
                {
                    sqlConnection.Open();
                    MessageBox.Show($"Connection : {sqlConnection.State}"); // состояние подключения, открылось или нет
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    sqlConnection.Dispose(); // закрываем соединение
                }
            }
        }


    }
}
